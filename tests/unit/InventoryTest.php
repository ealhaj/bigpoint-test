<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class InventoryTest extends TestCase
{
    public function testAddItem(): void
    {
        $inventory = new Inventory();

        $inventory->addItem('item1', 5);
        $this->assertEquals(5, $inventory->getStock('item1'));

        $inventory->addItem('item2', 3);
        $this->assertEquals(3, $inventory->getStock('item2'));

        $inventory->addItem('item1', 2);
        $this->assertEquals(7, $inventory->getStock('item1'));
    }

    public function testRemoveItem(): void
    {
        $inventory = new Inventory();

        $inventory->addItem('item1', 10);
        $inventory->removeItem('item1', 5);
        $this->assertEquals(5, $inventory->getStock('item1'));

        $inventory->removeItem('item2', 2);
        $this->assertEquals(5, $inventory->getStock('item1'));
    }

    public function testGetStock(): void
    {
        $inventory = new Inventory();

        $this->assertEquals(0, $inventory->getStock('item1'));

        $inventory->addItem('item1', 4);
        $inventory->addItem('item1', 3);
        $this->assertEquals(7, $inventory->getStock('item1'));
    }
}