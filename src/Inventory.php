<?php

declare(strict_types=1);

class Inventory
{
    private $stock = [];

    public function addItem($itemId, $amount): void
    {
        if (!isset($this->stock[$itemId])) {
            $this->stock[$itemId] = 0;
        }

        $this->stock[$itemId] += $amount;
    }

    public function removeItem($itemId, $amount): void
    {
        if (isset($this->stock[$itemId])) {
            $this->stock[$itemId] -= $amount;

            $this->stock[$itemId] = max(0, $this->stock[$itemId]);
        }
    }

    public function getStock($itemId): int
    {
        return $this->stock[$itemId] ?? 0;
    }
}
