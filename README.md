# Bigpoint Exam

## Prerequisites

Before you begin, ensure you have the following installed on your machine:

- PHP (>= 8.1)
- Composer

## Install & Test

1. **Install Dependencies:**
   Run the following command in your project directory to install dependencies:

   ```bash
   composer install

2. **Run the tests:**
    Run the following command in project root folder:

    ```bash
    ./vendor/bin/phpunit

3. **Run the `index.php`:**
    To run the vanilla php file and see it's output execute the command:

    ```bash
    php index.php